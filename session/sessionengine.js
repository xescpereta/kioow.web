﻿
var sessionEngine = function (req, res, next) {
    
    function _redirect_rules() {
        var _rt = null;
        if (req.path.indexOf('/affiliate/') > -1) {
            _rt = ((req.omtsession == null || req.omtsession.user == null) || !req.omtsession.user.isAffiliate) ? 
                '/private?redirecturl=' + url: 
                (req.omtsession.affiliate.membership.registervalid) ? null : '/account-invalid';
        }
        return _rt;
    }

    var _ = require('underscore');
    var session = null;
    var filterpaths = ['/img/', '/css/', '/fonts/', '/partials/', '/sass/', '/datadummy/', '/account-invalid', '/private', '/js/'];
    var filtered = _.filter(filterpaths, function (fpath) { return req.path.indexOf(fpath.toLowerCase()) > -1; });

    if (filtered != null && filtered.length > 0) { 
        next();
    }
    else {
        req.omtsession = (req.session != null && req.session.login != null) ? req.session.login : null;
        var url = encodeURIComponent(req.originalUrl);
        var imadmin = (req.omtsession != null && req.omtsession.user != null && req.omtsession.user.isAdmin);
        var redirection = imadmin ? null : _redirect_rules();

        console.log('redirect to -> %s', redirection);
        (redirection===null) ? next() : res.redirect(redirection);

    }
};

module.exports.sessionEngine = sessionEngine;