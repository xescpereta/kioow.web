﻿
//WEB SERVER Version 3.0.1 --- Clustering process

//Ready to create the worker process pool
var appPool = require('cluster');
//configure worker process for core
var workerCount = 1;

//Start the Pool
if (appPool.isMaster) {
    //configure and start the required worker process
    if (workerCount <= 0) {
        workerCount = require('os').cpus().length;
    }
    for (var i = 0; i < workerCount; i += 1) {
        appPool.fork();
    }
} else {
    // Commpression at top
    var compression = require('compression');
    //Initialize express dependencies and stuff...
    var express = require('express');
    
    var favicon = require('serve-favicon');
    var fs = require('fs');
    
    var http = require('http');
    var https = require('https');
    var path = require('path');
    var morgan = require('morgan');
    var swig = require('swig');
    var cookieParser = require('cookie-parser');
    var session = require('./session/sessionengine');
    var cache = require('./cache/cacheengine');
    var wwwredirect = require('./redirection/wwwredirect');
    var ytoapiclient = require('kioow.connector').expressconnector;
    var app = express();
    //Get the configuration file...
    
    var filepaths = require('./routes/diskfilepaths').filepaths;
    var nconf = require('nconf');
    var ex_session = require('express-session');
    var MongoStore = require('connect-mongo')(ex_session);
    
    nconf.env().file({ file: filepaths.configurationfile });
    var apisettings = nconf.get('kioowapiclient');
    // all environments
    app.set('port', nconf.get('port'));
    app.set('view engine', 'html');
    app.set('views', nconf.get('publicdirectory'));
    
    app.set('view cache', false);
    swig.setDefaults({ cache: false, varControls: ['{?', '?}'] });
    
    var bodyParser = require('body-parser');
    // compression at Top
    app.use(compression());
    app.use(wwwredirect.wwwredirect);
    app.use(bodyParser.urlencoded({ limit: '500mb', extended: false }));
    app.use(bodyParser.json({ limit: '500mb' }));
    app.use(morgan('dev'));
    app.use(favicon(nconf.get('publicdirectory') + '/images/favicon.ico'));

    app.use(cookieParser('kioow secret', { limit: '500mb' }));
    
    app.use(cache.cacheEngine);
    app.use(ex_session({
        secret: "kqsdjfmlksdhfhzirzeoibrzecrbzuzefcuercazeafxzeokwdfzeijfxcerig",
        store: new MongoStore({ url: 'mongodb://localhost:27017/operations-kioow' }),
        resave: false,
        saveUninitialized: true
    }));
    
    var methodOverride = require('method-override');
    app.use(methodOverride('X-HTTP-Method-Override'));
    //omt middleware
    app.use(ytoapiclient(apisettings.url, apisettings.endpointinterface));
    app.use(session.sessionEngine);
    app.use(express.static(nconf.get('publicdirectory'), {maxAge: 2 * 86400000}));
    app.uploadconfiguration = {
        cloudinary: nconf.get('cloudinaryconfig'),
        upload: nconf.get('uploads')
    };
    app.disable('etag');
    
    var needsAuth = nconf.get('basicAuth');
    if (needsAuth == "true") {
        var basicAuth = require('basic-auth-connect');
        app.use(basicAuth(function (user, pass) {
            return 'kioow' == user && 'kioow112' == pass;
        }));
    }
    
    var routes = require('./routes')(app);

    var cloudinary = require('cloudinary');
    cloudinary.config(app.uploadconfiguration.cloudinary);
    
    // set global filter Swig to change numbers decimal from dots to comma
    swig.setFilter('decimales', function (input) {
        var n = String(input);
        if (n.indexOf(".") >= 0) {
            var s = n.split('.');
            return s.join(',');
        } else {
            return input;
        }
    });
    // remove decimals number
    swig.setFilter('removeDecimal', function (nStr) {
        nStr = Math.round(nStr);
        var sep = '.';
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + sep + '$2');
        }
        return x1 + x2;
    });
    // divide 1000 as 1.000
    swig.setFilter('numberFractions', function (nStr) {
        var sep = '.';
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + sep + '$2');
        }
        return x1 + x2;
    });

    
    //Start Listening HTTP Requests...
    //HTTP Listening
    http.createServer(app).listen(app.get('port'), function () {
        console.log('Express server listening on port ' + app.get('port'));
    });
    //HTTPS Listening
    var ssl = nconf.get('ssl');
    if (ssl.enabled) {
        var ssloptions = {
            key: fs.readFileSync(ssl.keyfile),
            cert: fs.readFileSync(ssl.certfile),
        };
        https.createServer(ssloptions, app).listen(443, function () {
            console.log('Express server listening on SSL');
        });
    }
}

var dying = false;

appPool.on('exit', function (worker) {
    
    // Replace the dead worker,
    // we're not sentimental ... ;)
    if (dying == false) {
        console.log('Worker ' + worker.id + ' died :(');
        appPool.fork();
    }

});



process.on('SIGINT', function () {
    console.log('Got SIGINT.  Process exiting...');
    //redefine handlers...
    dying = true;
    appPool.on('exit', function () {
        console.log('Nothing to do. We are exiting process...');
    });
    //kill childs...
    for (var id in appPool.workers) {
        try {
            appPool.workers[id].kill('SIGINT');
        }
        catch (err) {
            console.log(err);
        }
    }
    process.exit(0);
});



