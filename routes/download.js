﻿
module.exports = function (app) {
    var cloudconfig = app.uploadconfiguration.cloudinary;
    var uploadconfig = app.uploadconfiguration.upload;
    var common = require('kioow.common');
    //Upload files management..
    
    var utils = common.utils;

    app.post('/download/getpdffromhtml', function (req, res) {
    	console.log('request html -> pdf');
        var fs = require('fs');
        var exec = require('child_process').exec;                
        var params = req.body;        
        var filepath = uploadconfig.path + params.type + '/' + params.namefile;
        var pathPDF = filepath + '.pdf';
        var pathHTML = filepath + '.html';
        var htmlcontent = params.html || '<html><head></head><body><h2>Test content</h2><p>this is a test content.. for testing pourposes</p></body></html>';
        var pageSettings = params.pageSettings || '--page-size A4';
        
        console.log("* pathHTML: ",pathHTML);
        console.log("* pathPDF: ",pathPDF);        
        console.log("* type file: ",params.type);
        console.log("* pageSettings: ",pageSettings);
        
        fs.writeFile(pathHTML, htmlcontent, function (err) {
            if (err) {
                console.log('ERROR saving file in html for file: ' + params.namefile + '. Details: ', err);
                res.send(null);
            } else {

                exec('wkhtmltopdf ' + pageSettings + ' ' + pathHTML + ' ' + pathPDF, function (file) {
                    console.log('fichero guardado en pdf');                    
                    console.log('borrar fichero html....');
                    fs.unlinkSync(pathHTML);

                    var fileurl = '/resources/file?file=' + params.namefile + '.pdf' + '&type=' + params.type;
                    
                    console.log('finished process [HTML -> PDF]. urls: ', fileurl);
                    var rtobject = { url: fileurl };
                    res.send(rtobject);
                });
            }
        }); 
    });

    app.get('/resources/file', function (req, res) {
        var type = req.query.type;
        var file = req.query.file;
        var subpath = type + '/' + file;
        var targetfile = uploadconfig.path + subpath;
        res.download(targetfile);
    });
}