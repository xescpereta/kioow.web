﻿
module.exports = function (app) {
    var cloudconfig = app.uploadconfiguration.cloudinary;
    var uploadconfig = app.uploadconfiguration.upload;
    var common = require('kioow.common');
    //Upload files management..
    var multer = require('multer');
    var utils = common.utils;
    
    var storageimages = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, uploadconfig.path + uploadconfig.cloudinary)
        },
        filename: function (req, file, cb) {
            var filename = utils.getToken() + '.' + utils.getfileextension(file.originalname);
            //the name...
            cb(null, filename)
        }
    });
    
    var storagevouchers = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, uploadconfig.path + uploadconfig.vouchers)
        },
        filename: function (req, file, cb) {
            var filename = utils.getToken() + '.' + utils.getfileextension(file.originalname);
            //the name...
            cb(null, filename)
        }
    });
    
    var storageinsurances = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, uploadconfig.path + uploadconfig.insurances);
        },
        filename: function (req, file, cb) {
            var filename = utils.getToken() + '.' + utils.getfileextension(file.originalname);
            //the name...
            cb(null, filename)
        }
    });
    
    var storagecertificates = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, uploadconfig.path + uploadconfig.certificates);
        },
        filename: function (req, file, cb) {
            var filename = utils.getToken() + '.' + utils.getfileextension(file.originalname);
            //the name...
            cb(null, filename)
        }
    });
    
    var storageinvoice = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, uploadconfig.path + uploadconfig.invoice);
        },
        filename: function (req, file, cb) {
            var filename = utils.getToken() + '.' + utils.getfileextension(file.originalname);
            //the name...
            cb(null, filename)
        }
    });
    
    var uploadimages = multer({ storage: storageimages });
    var uploadvouchers = multer({ storage: storagevouchers });
    var uploadinsurances = multer({ storage: storageinsurances });
    var uploadcertificates = multer({ storage: storagecertificates });
    var uploadinvoices = multer({ storage: storageinvoice });
    

    app.post('/upload/cloudinary', uploadimages.single('openmarketimages'), function (req, res) { 
        var file = req.file;
        var target_path = file.destination + '/' + file.filename;

        var cloudinary = require('cloudinary');
        cloudinary.config(cloudconfig);
        cloudinary.uploader.upload(target_path, function (result) {
            res.send(result);
        });
    });

    app.post('/upload/voucher', uploadvouchers.single('openmarketfile'), function (req, res) { 
        var file = req.file;
        var target_path = file.destination + '/' + file.filename;

        var url = '/resources/file?file=' + file.filename + '&type=voucher';
        var rsp = { url: url };

        res.send(rsp);

    });

    app.post('/upload/insurance', uploadvouchers.single('openmarketfile'), function (req, res) { 
        var file = req.file;
        var target_path = file.destination + '/' + file.filename;
        
        var url = '/resources/file?file=' + file.filename + '&type=insurances';
        var rsp = { url: url };
        
        res.send(rsp);
    });

    app.post('/upload/certification', uploadvouchers.single('openmarketfile'), function (req, res) { 
        var file = req.file;
        var target_path = file.destination + '/' + file.filename;
        
        var url = '/resources/file?file=' + file.filename + '&type=certifications';
        var rsp = { url: url };
        
        res.send(rsp);
    });

    app.post('/upload/invoice', uploadvouchers.single('openmarketfile'), function (req, res) { 
        var file = req.file;
        var target_path = file.destination + '/' + file.filename;
        
        var url = '/resources/file?file=' + file.filename + '&type=invoiceprovider';
        var rsp = { url: url };
        
        res.send(rsp);
    });
}