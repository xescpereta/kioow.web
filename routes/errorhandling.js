﻿module.exports = function (app) {

    //dependencies
    var utils = require('../utils');
    var _ = require('underscore');
    var filepaths = require('./diskfilepaths').filepaths;

    var settings = require('nconf');
    settings.env().file({ file: filepaths.configurationfile });

    var BRAND = settings.get('brand');

    swig = require('swig');


    app.use(function (req, res, next) {
        
        res.status(404);

        // respond with html page
        if (req.accepts('html')) {
            
            var errorcontent = {
                brand:          BRAND,
                session:        req.omtsession,    
                navigationType: ''
            };
            
            var tmpl = swig.compileFile(filepaths.static.error404);
            renderedHtml = tmpl(errorcontent);
            res.end(renderedHtml);
            return;
        }

        // respond with json
        if (req.accepts('json')) {
            res.send({ error: 'Not found' });
            return;
        }

        // default to plain-text. send()
        res.type('txt').send('Not found');
    });    

    app.get('/404', function (req, res, next) {
        next();
    });

    // ========== 403 ===========
    app.get('/403', function (req, res, next) {
        var err = new Error('not allowed!');
        err.status = 403;
        next(err);
    });

    // ========== 500 ===========
    app.get('/500', function (req, res, next) {
        next(new Error('keyboard cat!'));
    });


    console.log('loading error handling routes.. [OK]');
};

// app.use(function(err, req, res, next) {
//   if(err.status !== 404) {
//     res.status(404);
//     res.send(err.message || '** error happened **');
//         return next();
//   }
//  if(err.status !== 500) {
//     res.status(500);
//     res.send(err.message || '** error happened **');
//         return next();
//   }
  
// });
