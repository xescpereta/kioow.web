﻿'use strict';

module.exports = function (app) {
    
    //dependencies
    var utils = require('../utils');
    //internal WebServer Session Management
    app.get('/auth/recoverSession', function (req, res) {
        if (req.session != null && req.session.login != null) {
            res.send(req.session.login);
        } else { 
            res.send(null);
        }
    });
    
    app.post('/auth/recoverSession', function (req, res) {
        if (req.session != null && req.session.login != null) {
            res.send(req.session.login);
        } else {
            res.send(null);
        }
    });
    
    app.get('/auth/refreshsession', function (req, res) {
        if (req.session != null && req.session.login != null) {
            var refreshrequest = {
                command: 'refreshsession',
                service: 'membership',
                request: {
                    userid: req.session.login.user._id,
                    oncompleteeventkey : 'refreshsession.done',
                    onerroreventkey : 'refreshsession.error'
                }
            }
            var auth = {
                userid: req.session.login.user._id,
                accessToken: req.session.login.accessToken
            };
            refreshrequest.auth = auth;

            var rq = req.ytoconnector.send(refreshrequest);
            rq.on(refreshrequest.request.oncompleteeventkey, function (result) {
                console.log(result);
                //the login is success
                if (result != null) {
                    req.session.login = result.Session;
                    res.send(result.Session);
                }
                else {
                    res.status(500).send('empty response from api');
                }
            });
            
            rq.on(refreshrequest.request.onerroreventkey, function (err) {
                console.log(err);
                //the sign up is not success
                res.status(500).send(err);
            });
            
            rq.on('api.error', function (err) {
                console.log(err);
                res.status(500).send(err);
            });
            rq.on('api.timeout', function (tout) {
                console.log(tout);
                res.status(500).send(tout);
            });

        } else {
            res.send(null);
        }
    });
    
    app.post('/auth/refreshsession', function (req, res) {
        if (req.session != null && req.session.login != null) {
            console.log('lets refresh session...');
            var refreshrequest = {
                command: 'refreshsession',
                service: 'membership',
                request: {
                    userid: req.session.login.user._id,
                    oncompleteeventkey : 'refreshsession.done',
                    onerroreventkey : 'refreshsession.error'
                }
            }
            var auth = {
                userid: req.session.login.user._id,
                accessToken: req.session.login.accessToken
            };
            refreshrequest.auth = auth;

            var rq = req.ytoconnector.send(refreshrequest);
            rq.on(refreshrequest.request.oncompleteeventkey, function (result) {
                console.log(result);
                //the login is success
                if (result != null) {
                    req.session.login = result.Session;
                    res.send(result.Session);
                }
                else {
                    res.status(500).send('empty response from api');
                }
            });
            
            rq.on(refreshrequest.request.onerroreventkey, function (err) {
                console.log(err);
                //the sign up is not success
                res.status(500).send(err);
            });
            
            rq.on('api.error', function (err) {
                console.log(err);
                res.status(500).send(err);
            });
            rq.on('api.timeout', function (tout) {
                console.log(tout);
                res.status(500).send(tout);
            });

        } else {
            res.send(null);
        }
    });
    
    app.get('/auth/logout', function (req, res) {
        req.session.destroy(function (err) {
            if (err) {
                console.log(err);
                res.status(500).send(null);
            }
            else {
                res.send(null);
            }
        })
    });

    
    app.post('/auth/logout', function (req, res) {
        req.session.destroy(function (err) {
            if (err) {
                console.log(err);
                res.status(500).send(null);
            }
            else {
                res.send(null);
            }
        })
    });

    //Kioow - BACKBONE way Request
    app.post('/auth/logIn', function (req, res) {
        var loginrequest = req.body;
        console.log(loginrequest);
        loginrequest.oncompleteeventkey = 'login.done';
        loginrequest.onerroreventkey = 'login.error';
        var rqlgn = {
            command: 'login',
            request: loginrequest,
            service: 'membership',
        };
        var rq = req.ytoconnector.send(rqlgn);
        rq.on(loginrequest.oncompleteeventkey, function (result) {
            console.log(result);
            //the login is success
            if (result != null) {
                req.session.login = result.Session;
                res.send(result.Session);
            }
            else { 
                res.status(500).send('empty response from api');
            }
        });
        
        rq.on(loginrequest.onerroreventkey, function (err) {
            console.log(err);
            //the sign up is not success
            res.status(500).send(err);
        });
        
        rq.on('api.error', function (err) {
            console.log(err);
            res.status(500).send(err);
        });
        rq.on('api.timeout', function (tout) {
            console.log(tout);
            res.status(500).send(tout);
        });

    });
    
    app.post('/auth/validatetoken', function (req, res) {
        
        var validaterequest = {
            userid: '',
            accesstoken: ''
        };
        var sess = req.session.login;
        validaterequest.userid = sess.user._id;
        validaterequest.accesstoken = sess.accessToken;
        validaterequest.oncompleteeventkey = 'validatetoken.done';
        validaterequest.onerroreventkey = 'validatetoken.error';
        var rqlgn = {
            command: 'validatetoken',
            request: validaterequest,
            service: 'membership',
        };
        var rq = req.ytoconnector.send(rqlgn);
        rq.on(validaterequest.oncompleteeventkey, function (result) {
            console.log(result);
            //the validation is success
            res.send(result);
        });
        
        rq.on(validaterequest.onerroreventkey, function (err) {
            console.log(err);
            //the sign up is not success
            res.status(500).send(err);
        });
        
        rq.on('api.error', function (err) {
            console.log(err);
            res.status(500).send(err);
        });
        rq.on('api.timeout', function (tout) {
            console.log(tout);
            res.status(500).send(tout);
        });

    });
    
    app.post('/auth/signup', function (req, res) {
        var signuprequest = req.body;
        signuprequest.oncompleteeventkey = 'signup.done';
        signuprequest.onerroreventkey = 'signup.error';
        
        var rqsgn = {
            command: 'signup',
            request: signuprequest,
            service: 'membership',
        };
        
        var rq = req.ytoconnector.send(rqsgn);
        
        rq.on(signuprequest.oncompleteeventkey, function (result) {
            console.log(result);
            //the sign up is success
            req.session.login = result.Session;
            res.send(result.Session);
        });
        
        rq.on(signuprequest.onerroreventkey, function (err) {
            console.log(err);
            //the sign up is not success
            res.status(500).send(err);
        });
        
        rq.on('api.error', function (err) {
            console.log(err);
            res.status(500).send(err);
        });
        rq.on('api.timeout', function (tout) {
            console.log(tout);
            res.status(500).send(tout);
        });

    });


    
}