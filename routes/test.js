module.exports = function (app) {
    'use strict';
    //dependencies
    var utils = require('../utils');
    var filepaths = require('./diskfilepaths').filepaths;
    var _ = require('underscore');
    var common = require('yourttoo.common');
    var cloudinary = require('cloudinary');

    var settings = require('nconf');
    settings.env().file({ file: filepaths.configurationfile });
    var BRAND = settings.get('brand');
    
//
//
//
//
//  TESTS
//
//
//
//


    app.get('/test', function (req, res, next) {
        
        //console.log ('es private ');
        
        var content = {
            brand: BRAND,
            session: req.omtsession,    
            navigationType: '',
            redirectURL : req.param('url')
        };

        var tmpl = swig.compileFile(filepaths.test);
        var renderedHtml = tmpl(content);
        res.end(renderedHtml);
        
    });


    app.get('/test/pdfs', function (req, res, next) {
        
        console.log ('test pdfs url ');
        
        var content = {
            brand: BRAND,
            redirectURL :       req.query.url,
            vat:                '', 
            local:              {            
                priceperson : 0,
                pax: 10,
                subtotalpax: 0,
                subtotal: 0,
                total: 0,
                dmcvat: 0, // importe sobre el total, aplicando el vat
                vat: 0,   //valor vat que ponen en la factura
                omtdis: 0,
                omt: 0,
                topay: 1000,                   
                amountProvider: null,
                invocenumber: 'exemplo123'
            },
            dmc:                {},
            dmccode:            req.query.dmccode,
            dmcproduct:         {},
            dmcproductcode:     req.query.dmcproductcode,
            booking:            {},
            idBooking:          req.query.idBooking,
            title:              "TEST",
            getUrlCloudinary:   utils._cloudinary_url,                
            description:        "Tu herramienta para los viajes multidays ONLINE y A MEDIDA",
            url:                filepaths.rooturl + utils._getUrlBrand(req.route.path),            
            session:            req.omtsession,
            navigationType:     'affiliate',
            currency:           {"label": "Euro", "symbol": "€", "value": "EUR"},
            fees:               {"unique" : 0, "groups" : 0, "tailormade" : 0, "flights" : 0}  
        };
        

        //var postdata = { local: local, dmcproduct : dmcproduct, booking : booking}
        function _getBooking(callback){
            var request = {
                collectionname : 'Bookings',
                query : {
                    'idBooking' : content.idBooking
                },
                populate: [{path: 'affiliate'}]
            };          
            var command = 'findone';
            request.oncompleteeventkey = command + '.done';
            request.onerroreventkey = command + '.error';
            
            var rqCMD = {
                command: command,
                request: request,
                service: 'core',
            };
            
            // obtener la session
            if (req.session != null && req.session.login != null) {
                var auth = {
                    userid: req.session.login.user._id,
                    accessToken: req.session.login.accessToken
                };
                rqCMD.auth = auth;
            }
            
            var rq = req.ytoconnector.send(rqCMD);

            //request success
            rq.on(request.oncompleteeventkey, function (result) {            
                if (result != null){
                    content.booking = result;    
                    content.dmcproduct = JSON.parse(content.booking.product);
                    content.dmcproductcode = content.dmcproduct.code;
                    content.dmccode = content.dmcproduct.dmccode;
                    if (callback){
                        callback();
                    }
                } else {
                    console.log ('findDMCByCode 1 An unknown error has ocurred in results. No results.');
                    res.status(500).send('find DMC By Code An unknown error has ocurred in results. No results.');
                }               
            });
            //request is not success
            rq.on(request.onerroreventkey, function (err) {
                console.log(err);
                res.status(500).send(err);
            });
            
            //request error access to api...
            rq.on('api.error', function (err) {
                console.log(err);
                res.status(500).send(err);
            });
            
            //request timeout api not responding...
            rq.on('api.timeout', function (tout) {
                console.log(tout);
                res.status(503).send("Server too busy right now, sorry.");
            });
        }


        var _getDmc = function(callback){

            var request = {
                collectionname : 'DMCs',
                query : {
                    //'code' : content.quote.dmccode
                    'code' : content.dmccode
                }
            };          
            var command = 'findone';
            request.oncompleteeventkey = command + '.done';
            request.onerroreventkey = command + '.error';
            
            var rqCMD = {
                command: command,
                request: request,
                service: 'core',
            };
            
            // obtener la session           
            if (req.session != null && req.session.login != null) {
                var auth = {
                    userid: req.session.login.user._id,
                    accessToken: req.session.login.accessToken
                };
                rqCMD.auth = auth;
            }
            
            var rq = req.ytoconnector.send(rqCMD);

            //request success
            rq.on(request.oncompleteeventkey, function (result) {            
                if (result != null){
                    content.dmc = result;                   
                    if (callback){
                        callback();
                    }
                } else {
                    console.log ('findDMCByCode 1 An unknown error has ocurred in results. No results.');
                    res.status(500).send('find DMC By Code An unknown error has ocurred in results. No results.');
                }               
            });
            //request is not success
            rq.on(request.onerroreventkey, function (err) {
                console.log(err);
                res.status(500).send(err);
            });
            
            //request error access to api...
            rq.on('api.error', function (err) {
                console.log(err);
                res.status(500).send(err);
            });
            
            //request timeout api not responding...
            rq.on('api.timeout', function (tout) {
                console.log(tout);
                res.status(503).send("Server too busy right now, sorry.");
            });
        };

        var _getProductDmc = function(callback){

            var request = {
                collectionname : 'DMCProducts',
                query : {
                    //'code' : content.dmcproductcode
                    'code' : 'OMT3041604'
                },
                populate: [
                {path: 'departurecity'},
                {path: 'stopcities'},
                {path: 'sleepcity'},
                {path: 'departurecountry'},
                {path: 'stopcountry'},
                {path: 'sleepcountry'},
                {path: 'dmc'}
                ]
            };          
            var command = 'findone';
            request.oncompleteeventkey = command + '.done';
            request.onerroreventkey = command + '.error';
            
            var rqCMD = {
                command: command,
                request: request,
                service: 'core',
            };
            
            // obtener la session           
            if (req.session != null && req.session.login != null) {
                var auth = {
                    userid: req.session.login.user._id,
                    accessToken: req.session.login.accessToken
                };
                rqCMD.auth = auth;
            }
            
            var rq = req.ytoconnector.send(rqCMD);

            //request success
            rq.on(request.oncompleteeventkey, function (result) {   
                if (result != null){
                    content.product = result;
                    if (content.product.minprice !== null && content.product.minprice !== undefined){
                        
                        //console.log("++++++++ content.product.minprice: ",content.product.minprice);
                        //
                        // call _buildAffiliateMinPrice
                        // 
                        var minPriceData = {
                            minPrice : content.product.minprice,
                            currency : content.currency,
                            fees : content.fees
                        };
                        //console.log("++++++++ minPriceData: ",minPriceData);
                        content.minPrice = utils._buildAffiliateMinPrice(minPriceData);
                        //console.log("++++++++ utils._buildAffiliateMinPrice(minPriceData): ",utils._buildAffiliateMinPrice(minPriceData));

                        //
                        if (content.minPrice !== null && content.minPrice.pvp > 0) {
                            content.disponible = true;
                            content.dayprice = utils._calculateDayPrice(content.minPrice.pvp, 
                                content.minPrice.currency, content.product.itinerary);
                        }
                    }
                    //
                    content.citysAndHotelCats =  utils._getCitiesAndHotelCats(content.product.itinerary);
                    //
                    content.minPrice.pvp = 1000;
                    content.countries = [];
                    for (var i = result.sleepcountry.length - 1; i >= 0; i--) {
                        console.log ('result.sleepcountry[i] ',result.sleepcountry[i]);
                        content.countries.push(result.sleepcountry[i]);
                    };                
                    if (callback){
                        callback();
                    }
                } else {
                    console.log ('DMCProducts 1 An unknown error has ocurred in results. No results.');
                    res.status(500).send('find DMCProducts By Code An unknown error has ocurred in results. No results.');
                }               
            });
            //request is not success
            rq.on(request.onerroreventkey, function (err) {
                console.log(err);
                res.status(500).send(err);
            });
            
            //request error access to api...
            rq.on('api.error', function (err) {
                console.log(err);
                res.status(500).send(err);
            });
            
            //request timeout api not responding...
            rq.on('api.timeout', function (tout) {
                console.log(tout);
                res.status(503).send("Server too busy right now, sorry.");
            });
        };
       
        var render = function(){
            // factura
            //var tmpl = swig.compileFile(filepaths.affiliate.ytoinvoicetoprint);
            // resumen de reserva
            //var tmpl = swig.compileFile(filepaths.affiliate.ytobookingsummarytoprint);
            // bono
            //var tmpl = swig.compileFile(filepaths.affiliate.ytovouchertoprint);
            // product para cliente
            //var tmpl = swig.compileFile(filepaths.affiliate.producttoprint);
            // product carteleria
            var tmpl = swig.compileFile(filepaths.affiliate.producttoprintonepage);
            var renderedHtml = tmpl(content);
            res.end(renderedHtml);
        };
        
        var init = function(){
            //_getBooking(function (){
                console.log ('content.idBooking ',content.idBooking);
                content.product = content.dmcproduct;
                //para probar itinerarios y resumen una página
                content.dmcproductcode = 'ES112258';
                // para probar con invoice definitiva
                //content.booking.invoiceAffiliate = "EXT3434343";
                _getProductDmc(function(){
                    //_getDmc(function(){
                        render();
                    //});
                });
           // });
        }        
        init();
    });




};
