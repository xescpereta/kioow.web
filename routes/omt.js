﻿module.exports = function (app) {

    //dependencies
//     var utils = require('../utils');
//     var filepaths = require('./diskfilepaths').filepaths;
    
//     app.get('/omt/apidoc', function (req, res) {
//         var tmpl = swig.compileFile(filepaths.omt.apidoc);
//         var renderedHtml = tmpl();
//         res.end(renderedHtml);
//     });

//     // ========== OMT-LIST / omt-list ===========
    
//     // Vista de control traer todos los dmc y mostrarlos en una lista
//     app.get('/omt-list/dmc/:page?/', function(req, res, next){
//       // console.log("[get] /omt-list/"+req.param("page"));
//         var filters = "";
//         var countries = [];
//         var channels = [];
//         var segments = [];
//         var status = [];

//         var querystr = req.query;
//         //console.log(req.route)



//         if (req.omtsession == null || req.omtsession.user == null || !req.omtsession.user.isAdmin) {
//             res.redirect('/omt');
//             return;
//         }

//         if (req.param("countries") == '' || req.param("countries") == undefined){
//             countries = [];
//             countries = "";
//         } else{
//             countries = req.param("countries").split(",");
//         }

//         if (req.param("channels") == '' || req.param("channels") == undefined){
//             channels = [];
//             channels = "";
//         } else{
//             channels = req.param("channels").split(",");
//         }

//         if (req.param("segments") == '' || req.param("segments") == undefined){
//             segments = [];
//             segments = "";
//         } else{
//             segments = req.param("segments").split(",");
//         }

//         if (req.param("status") == '' || req.param("status") == undefined){
//             status = [];
//             status = "";
//         } else{
//             status = req.param("status").split(",");
//         }
//         console.log(req.param("page"), 'page1')
//         var page = 1;
//         if (req.param("page") == undefined || req.param("page") == 0){
//           page = 1;
//         } else{
//           page = req.param("page");
//         }

//         //console.log("page: "+page)

//         var tmpl = swig.compileFile(filepaths.omt.dmclist);

//         var dlcontent = {
//             dmclist : {},
//             title : "OMT Admin - DMC List ",
//             login : false,
//             language : "_es",
//             filters: filters,
//             filterslist : {
//                 countries : []
//             },
//             session: req.omtsession,
//             navigationType: 'admin'
//         }
        
//         var spl = req.originalUrl.split('?');
//        // console.log(spl[1]);
//         dlcontent.querystring = spl[1];

//         console.log("get cache DMCs");
//         req.omtcache.methods.recover('DMCsCACHE', function(results){
//             if (results) {
//                var totalItems = results.Item;
//                dlcontent.dmcsnumber = totalItems.length;
//                dlcontent.filterslist = utils._getFiltersDMCs(totalItems);
//                dlcontent.filterslist.filterCountries = utils._getOperationCountries(totalItems);
//                var filterItems = [];
//                // check filters exist
//                if (status.length > 0 || countries.length > 0 || channels.length > 0  || segments.length > 0){
//                     // iterate all dmcs
//                     for (var i =0; i < totalItems.length; i++){
//                         // default no match
//                         var nomatch = true;


//                         // Countries filters
//                         if (countries.length > 0){
//                            // console.log("entra en countries")
//                             for (var j = 0; j< totalItems[i].company.operatein.length; j++){
//                                 //console.log ("nomatch : ",nomatch)
//                                 if (nomatch){
//                                     for (var f = 0; f<countries.length; f++){
//                                         if (totalItems[i].company.operatein[j].operateLocation.countrycode == countries[f]){
//                                             nomatch = false;
//                                             //console.log("match")
//                                             break
//                                         } else {
//                                             nomatch = true;
//                                         }
//                                     } // END for countriesfilters
//                                 } else {
//                                     break
//                                 }
//                             } // END for operate locations
//                         } else {
//                             nomatch = false;
//                         }


//                          // Status filters
                        
//                         if (!nomatch && status.length > 0){
//                             //console.log("entra en status")
//                             for (var j = status.length - 1; j >= 0; j--) {
//                                 var mem = totalItems[i].membership;
//                                 if (status[j] == "waiting"){
//                                     if (!mem.registervalid){
//                                         nomatch = false;
//                                         break
//                                     } else {
//                                         nomatch = true;
//                                     }
//                                 } else if (status[j] == "valid"){
//                                     if (mem.registervalid && mem.publicprofilecomplete && mem.companyimagescomplete && mem.companycomplete && mem.paymentcomplete && mem.companytaxid && mem.emergencycomplete ){
//                                         nomatch = false;
//                                         break
//                                     } else{
//                                         nomatch = true;
//                                     }
//                                 } else if (status[j] == "confirmed"){
//                                     if (mem.registervalid){
//                                         if (!mem.publicprofilecomplete || !mem.companyimagescomplete || !mem.companycomplete || !mem.paymentcomplete || !mem.companytaxid || !mem.emergencycomplete){
//                                             nomatch = false;
//                                             break
//                                         } else{
//                                             nomatch = true;
//                                         }
//                                     } else {
//                                         nomatch = true;
//                                     }
//                                 }
                                
//                             };
//                         }




//                          // Channel filters
                        
//                         if (!nomatch && channels.length > 0){

//                             for (var j = channels.length - 1; j >= 0; j--) {

//                                 if ( totalItems[i].membership[channels[j]]){
//                                     nomatch = false;
//                                     break
//                                 } else {
//                                     nomatch = true;
//                                 }
//                             };
//                         }

//                         // Segments filters

//                         if (!nomatch && segments.length > 0){

//                             for (var j = segments.length - 1; j >= 0; j--) {

//                                 if ( totalItems[i].membership[segments[j]]){
//                                     nomatch = false;
//                                     break
//                                 } else {
//                                     nomatch = true;
//                                 }
//                             };
//                         }

//                         // all filter passed
//                         if(!nomatch){
//                             filterItems.push(totalItems[i]);
//                         }

//                     } // END for DMCS
//                } else { // No countries filters show all DMCs
//                     filterItems = results.Item;
//                } //

//                dlcontent.dmcsfilterednumber = filterItems.length;
//                //pagination results
//                var itemspage = 20; // ITEMS Per Page
//                var pg = require('../utils/pager');
//                var pager = new pg.Pager(filterItems, null, null, itemspage);

//                 dlcontent.dmclist = pager;
//                 dlcontent.pages = pager.Pages.length;
//                 dlcontent.activePage = parseInt(page);

//                 if (pager.Pages[page - 1] == undefined){
//                     dlcontent.pageresults = []
//                 }else{
//                     dlcontent.pageresults = pager.Pages[page - 1].items;
//                 }
//                 //res.json(dlcontent.pageresults)
//                 renderedHtml = tmpl(dlcontent);
//                 res.end(renderedHtml);
//             } else{
//                 console.log('An unknown error has ocurred in DMCsCACHE');
//                 next();
//             }

//         });


//     });

// //////////////////////////////
// // ========== OMT-LIST / Filter countries ===========
    
//     app.get('/omt-list/getDmcCountries', function(req, res, next){
//         var content = {}

//         if (req.omtsession == null || req.omtsession.user == null || !req.omtsession.user.isAdmin) {
//             res.redirect('/omt');
//             return;
//         }

//         var url = utils.api + '/dmc/getalldmcs';
//        utils.http.Post(url, {countries : ""}, utils.headers, function (results) {
//             if (results) {
//                 if (results.responseBody) {
//                     content.dmclist = results.responseBody;
//                     content = utils._getOperationCountries(content.dmclist);
//                     res.json(content);
//                 } else {
//                     console.log('An unknown error has ocurred in responseBody');
//                     next();
//                 }
//             } else {
//                 console.log('An unknown error has ocurred in results');
//                 next();
//             }
//         });

//     });
    
//     ///////////////////////////

//     app.get('/omt-list/product/:page?', function (req, res, next) {
//         // console.log("[get] /omt-list/"+req.param("page"));
//         var page = 1;

//         if (req.omtsession == null || req.omtsession.user == null || !req.omtsession.user.isAdmin) {
//             res.redirect('/omt');
//             return;
//         }

//         if (req.param("page") == undefined || req.param("page") == 0) {
//             page = 1;
//         } else {
//             page = req.param("page");
//         }
//         var dlcontent = {
//             productlist: {},
//             title: "OMT Admin - DMC List ",
//             login: false,
//             language: "es",
//             session: req.omtsession,
//             navigationType: 'admin'
//         };
//         var tmpl = swig.compileFile(filepaths.omt.productlist);
//         var renderedHtml = tmpl(dlcontent);
//         res.end(renderedHtml);
        
        

//     });
    

//     app.get('/omt-list/traveler/:page?', function (req, res, next) {
//         console.log("[get] /omt-list/"+req.param("page"));

//         console.log("req.query dateorder", req.param('dateorder'));

//         if (req.omtsession == null || req.omtsession.user == null || !req.omtsession.user.isAdmin) {
//             res.redirect('/omt');
//             return;
//         }

//         var page = 1;
//         if (req.param("page") == undefined || req.param("page") == 0) {
//             page = 1;
//         } else {
//             page = parseInt(req.param("page"));
//         }

//         var tmpl = swig.compileFile(filepaths.omt.travelerlist);
//         var dlcontent = {
//             getUrlCloudinary: utils._cloudinary_url,
//             travelerlist: {},
//             title: "OMT Admin - DMC List ",
//             login: false,
//             language: "es",
//             session: req.omtsession,
//             navigationType: 'admin'
//         }
//         var url = utils.api + '/omt/getalltravelers';
//         var dateorder = '';
//         if (req.param('dateorder') == null && req.param("dateorder") == undefined ){
//             dateorder = 'desc';
//         }else{
//             dateorder = req.param('dateorder');
//         }
//         var sendObj = {'dateorder' : dateorder}
//         utils.http.Get(url, sendObj, utils.headers, function (results) {
//             if (results) {
//                 if (results.responseBody) {
//                     dlcontent.travelerlist = JSON.parse(results.responseBody);
//                     console.log(dlcontent.travelerlist.Item.TotalItems>0);
//                     if (dlcontent.travelerlist.Item.TotalItems>0){
//                         dlcontent.travelerlist.pages = dlcontent.travelerlist.Item.Pages.length;
//                         dlcontent.travelerlist.activePage = parseInt(page);
//                         dlcontent.travelerlist.pageresults = dlcontent.travelerlist.Item.Pages[page - 1].items;

//                         for (var i = 0; i < dlcontent.travelerlist.pageresults.length ; i++) {
                            
//                             // TODO, DELETE next 2 when dmc.images.logo comes with cloudinary object in JSON
//                             if (dlcontent.travelerlist.pageresults[i].images &&
//                                 dlcontent.travelerlist.pageresults[i].images.logo) {
//                                 dmcurlid = dlcontent.travelerlist.pageresults[i].images.logo.url;
//                                 dlcontent.travelerlist.pageresults[i].images.logo.public_id =
//                                     dmcurlid.substr(dmcurlid.lastIndexOf('/') + 1, 20);
//                             }
//                             // Stop deleting
//                         }
//                     };
//                     renderedHtml = tmpl(dlcontent);
//                     res.end(renderedHtml);
//                 } else {
//                     throw 'An unknown error has ocurred in responseBody';
//                     next();
//                 }
//             } else {
//                 throw 'An unknown error has ocurred in results';
//                 next();
//             }
//         });

//     });
    
//     app.get('/omt-list/members', function (req, res) {
//         var tmpl = swig.compileFile(filepaths.omt.memberslist);
//         var dlcontent = {
//             dmclist: {},
//             title: "OMT Admin - Members list",
//             login: false,
//             language: "es",
//             session: req.omtsession,
//             navigationType: 'admin'
//         };
//         console.log(req.omtsession);
//         if (req.omtsession != null && 
//             req.omtsession.user != null && 
//             req.omtsession.user.isAdmin) {
//             var renderedHtml = tmpl(dlcontent);
//             res.send(renderedHtml);
//         } else {
//             dlcontent.session = null;
//             res.redirect('/omt-home');
//         }
        
//     });

//     // ========== OMT-HOME ==============
//     app.get('/omt', function (req, res) {
//         var tmpl = swig.compileFile(filepaths.omt.landing);
//         var dlcontent = {
//             dmclist: {},
//             title: "OMT Admin - Home",
//             login: false,
//             language: "es",
//             session: req.omtsession,
//             navigationType: 'admin'
//         };
//         //console.log(req.omtsession);
//         if (req.omtsession != null && req.omtsession.user != null && !req.omtsession.user.isAdmin) {
//             res.redirect('/omt-home');
//         } else { 
//             dlcontent = {
//             title: "Zona Privada",
//             login: false,
//             language: "es",
//             session: null,
//             navigationType: 'traveler'
//             };
//             var renderedHtml = tmpl(dlcontent);
//             res.send(renderedHtml);
//         }
        
//     });

//     app.get('/omt-products-data', function (req, res) {
//         var api = utils.api;
//         var url = api + '/omt/getAllProducts';
//         var jsonresponse = {};
//         utils.http.Get(url, "", utils.headers, function (results) {
//             if (results) {
//                 if (results.responseBody) {
//                     jsonresponse = JSON.parse(results.responseBody);
//                     for (var i = 0; i < jsonresponse.Item.Pages.length; i++) {
//                         for (var k = 0; k < jsonresponse.Item.Pages[i].items.length; k++) {
//                             var dmc = 
//                             {
//                                 name : jsonresponse.Item.Pages[i].items[k].dmc.name, 
//                                 code : jsonresponse.Item.Pages[i].items[k].dmc.code
//                             }
//                             jsonresponse.Item.Pages[i].items[k].dmc = null;
//                             jsonresponse.Item.Pages[i].items[k].dmc = dmc;
//                             jsonresponse.Item.Pages[i].items[k].availability = null;
//                             jsonresponse.Item.Pages[i].items[k].keys = null;
//                             jsonresponse.Item.Pages[i].items[k].description_en = null;
//                             jsonresponse.Item.Pages[i].items[k].description_es = null;
//                         }
//                     };
//                     res.send(jsonresponse);
//                 }
//             }
//         });
        
//     });
    
//     app.get('/omt-products-data-cache', function (req, res) {
//         var api = utils.api;
//         var url = api + '/omt/getAllProducts';
//         var jsonresponse = {};
//         req.omtcache.methods.recover('ProductsCACHE', function (rsp) { 
//             res.send(rsp);
//         });
        
//     });

//     app.get('/omt-home', function (req, res) {
//         var tmpl = swig.compileFile(filepaths.omt.home);

//         if (req.omtsession == null || req.omtsession.user == null || !req.omtsession.user.isAdmin) {
//             res.redirect('/omt');
//             return;
//         }

//         var dlcontent = {
//             dmclist: {},
//             title: "OMT Admin - Home",
//             login: false,
//             language: "es",
//             session: req.omtsession,
//             navigationType: 'admin'
//         };
//         var renderedHtml = tmpl(dlcontent);
//         res.send(renderedHtml);
//     });

//     app.get('/omt-list/booking', function (req, res, next) {
//          var tmpl = swig.compileFile(filepaths.omt.bookingslist);

//          if (req.omtsession == null || req.omtsession.user == null || !req.omtsession.user.isAdmin) {
//              res.redirect('/omt');
//              return;
//          }

//          var dlcontent = {
//              dmclist: {},
//              title: "OMT Lista de reservas",
//              login: false,
//              language: "es",
//              session: req.omtsession,
//              navigationType: 'admin'
//          };
//          var renderedHtml = tmpl(dlcontent);
//          res.send(renderedHtml);
        
//     });

//     app.get('/omt-list/requests', function (req, res, next) {
//         var tmpl = swig.compileFile(filepaths.omt.requestslist);

// //        
// //        if (req.omtsession == null  || !req.omtsession.user.isAdmin) {
// //            res.redirect('/omt');
// //            return;
// //        }

//         var dlcontent = {
//             title: "OMT: lista de peticiones a medida",
//             login: false,
//             language: "es",
//             session: req.omtsession,
//             navigationType: 'admin'
//         };
//         var renderedHtml = tmpl(dlcontent);
//         res.send(renderedHtml);

//     });
    
//     /**
//      * para ver las respuesta a las request taylormade
//      */
//     app.get('/omt-response', function (req, res) {
//         var content = {
//             bookinglist: {},
//             language: "es",
//             title: "Request Details " + req.param("code"),
//             queryCode : req.param("code"),
//             description: "Request Taylor Made - Online Booking Platform",
//             url: "http://www.openmarket.travel/omt-response",
//             session: req.omtsession,
//             navigationType: 'admin',
//             serverDate : new Date()
//         }                 
        
//         var tmpl = swig.compileFile(filepaths.omt.response);
//         var renderedHtml = tmpl(content);
//         res.send(renderedHtml);
//     });

//     app.get('/omt-client-account', function (req, res) {

//         var content = {
//             language : "es",
//             title: "Cuenta de ",
//             session: req.omtsession,
//             navigationType: 'admin'
//         }
//         var tmpl = swig.compileFile(filepaths.omt.clientaccount);
//         renderedHtml = tmpl(content);
//         res.end(renderedHtml);
        
//     });

}