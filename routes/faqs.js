module.exports = function (app) {

    //dependencies
    var utils = require('../utils');
    var _ = require('underscore');
    var filepaths = require('./diskfilepaths').filepaths;

    var settings = require('nconf');
    settings.env().file({ file: filepaths.configurationfile });
    
    var BRAND = settings.get('brand');

    var common_content = {
        name:       settings.get("brand"),
        brand:      settings.get("brand"),
        phone:      settings.get("phone"),
        logo:       settings.get("logo"),
        logox2:     settings.get("logox2"),
        lang:       'es',
        profileURL: '/affiliate'
    };

    // ========== FAQS / faqs ===========
    app.get('/faqs', function (req, res, next) {
        
        res.set('Content-Type', 'text/html');
        
        var faqscontent = {
            brand:          BRAND,
            canonicalurl:   filepaths.rooturl + utils._getUrlBrand(req.route.path),
            url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            session:        req.omtsession,
            navigationType: 'affiliate',
            title:          'FAQs - ' + BRAND.domain,
            description :   'FAQs - ' + BRAND.domain,
            faqs: {}
        };

        // comentar la siguiente linea a la api de produción
        var api = utils.api;

        var tmpl = swig.compileFile(filepaths.faqs.faqs);
        var url = api + '/cms/getAffiliateFaqs?debug=true';
        var data = { };

        utils.http.Get(url, data, utils.headers, function (results) {
            if (results) {
                if (results.responseBody) {
                    faqscontent.faqs = JSON.parse(results.responseBody);                
                    renderedHtml = tmpl(faqscontent);
                    res.end(renderedHtml);
                }
                else {
                    console.log("An unknown error has ocurred in responseBody");
                    next();
                }
            }
            else {
                console.log("An unknown error has ocurred in results");
                next();
            }
        });

    });
    
    // ========== FAQ TO FAQS REDIRECTION / faq ===========
    app.get('/faq', function(req, res) {
        res.redirect('/faqs');
    });


    // ========== FAQ / faq/slug ===========
    app.get('/faq/:slug?', function (req, res, next) {
        
        res.set('Content-Type', 'text/html');
        
        var faqcontent = {
            brand:              BRAND,
            canonicalurl:       filepaths.rooturl+"/faqs/"+req.param("slug"),
            // canonicalurl:   filepaths.rooturl + utils._getUrlBrand(req.route.path),
            // url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            session:            req.omtsession,
            navigationType:     'affiliate',
            title:              'FAQ - ' + BRAND.domain,
            description :       'FAQ - ' + BRAND.domain,
            getUrlCloudinary:   utils._cloudinary_url,
            login:              false,
            faq:                {}
        };

        
        // TODO CAMBIO DE URL PARA PRODUCCION 
        // comentar la siguiente linea a la api de produción
        var api = utils.api;

        var tmpl = swig.compileFile(filepaths.faqs.faq);
        var url = api + '/cms/getAffiliateFaqs';
        var data = { "faq": req.param("slug")};

        utils.http.Get(url, data, utils.headers, function (results) {
            if (results) {
                if (results.responseBody) {
                    faqcontent.faq = JSON.parse(results.responseBody);
                    console.log(faqcontent.faq);
                    renderedHtml = tmpl(faqcontent);
                    res.end(renderedHtml);
                }
                else {
                    console.log("An unknown error has ocurred in responseBody");
                    next();
                }
            }
            else {
                console.log("An unknown error has ocurred in results");
                next();
            }
        });

    });

    // ========== FAQ CATEGORY / faq/cat ===========
    app.get('/faqs/:cat?', function (req, res, next) {

        res.set('Content-Type', 'text/html');

        var faqcatcontent = {
            brand:          BRAND,
            // canonicalurl:   filepaths.rooturl + utils._getUrlBrand(req.route.path),
            canonicalurl:   filepaths.rooturl+"/faqs/"+req.param("cat"),
            url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            session:        req.omtsession,
            navigationType: 'affiliate',
            title:          'FAQs - ' + BRAND.domain,
            login:          false,
            faqcat:         {}
        };
        
        // comentar la siguiente linea a la api de produción
        var api = utils.api;

        var tmpl = swig.compileFile(filepaths.faqs.faqcat);
        var url = api + '/cms/getAffiliateFaqs';
        var data = { "category": req.param("cat")};

        utils.http.Get(url, data, utils.headers, function (results) {
            if (results) {
                if (results.responseBody) {
                    faqcatcontent.faqcat = JSON.parse(results.responseBody);                
                    faqcatcontent.faqcat = faqcatcontent.faqcat[0];
                    renderedHtml = tmpl(faqcatcontent);
                    res.end(renderedHtml);
                }
                else {
                    console.log("An unknown error has ocurred in responseBody");
                    next();
                }
            }
            else {
                console.log("An unknown error has ocurred in results");
                next();
            }
        });

    });

    // ========== FAQ SEARCH / faq/ ===========
    app.get('/faqs/search/:search?', function (req, res, next) {
        // http://openmarketdev.cloudapp.net:3000/cms/getDmcFaqs?category=how-can-i-become-a-supplier
        res.set('Content-Type', 'text/html');
        var faqsearchcontent = {
            brand:          BRAND,
            // canonicalurl:   filepaths.rooturl + utils._getUrlBrand(req.route.path),
            canonicalurl:   filepaths.rooturl+"/faqs",
            url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            title:          'FAQs - ' + BRAND.domain,
            description:    'FAQs - ' + BRAND.domain,
            login:          false,
            faqsearch:      {},
            search:         req.param("search"),
            session:        req.omtsession,
            navigationType: 'affiliate'
        };
        
        // comentar la siguiente linea a la api de produción
        var api = utils.api;

        var tmpl = swig.compileFile(filepaths.faqs.faqs);
        var url = api + '/cms/getAffiliateFaqs';
        var data = { "search": req.param("search")};

        utils.http.Get(url, data, utils.headers, function (results) {
            if (results) {
                if (results.responseBody) {
                    faqsearchcontent.faqsearch = JSON.parse(results.responseBody);                
                    // faqsearchcontent.faqsearch = faqsearchcontent.faqsearch[0];
                    renderedHtml = tmpl(faqsearchcontent);
                    res.end(renderedHtml);
                }
                else {
                    console.log("An unknown error has ocurred in responseBody");
                    next();
                }
            }
            else {
                console.log("An unknown error has ocurred in results");
                next();
            }
        });
    });



};
