﻿module.exports = function (app) {
    
    require('./home')(app);
    require('./backbone')(app);
    require('./auth')(app);
    require('./download')(app);
    require('./upload')(app);
    require('./statics')(app);
    require('./faqs')(app);
	require('./admin')(app);
    require('./errorhandling')(app);
}