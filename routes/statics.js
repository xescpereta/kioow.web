module.exports = function (app) {
    'use strict';
    //dependencies
    var utils = require('../utils');
    var filepaths = require('./diskfilepaths').filepaths;
    var _ = require('underscore');
    var common = require('kioow.common');
    var cloudinary = require('cloudinary');

    var settings = require('nconf');
    settings.env().file({ file: filepaths.configurationfile });
    var BRAND = settings.get('brand');
    
    app.get('/statics/getfullstaticcontent', function (req, res) {
        res.set('Content-Type', 'application/json');
        res.send(
            {
                currency: common.staticdata.currencys,
                timezones: common.staticdata.timezones,
                bankcountries: common.staticdata.bankcountries,
                assistancelanguages: common.staticdata.assistancelanguages,
                paymentoptions: common.staticdata.paymentoptions
            });
    });
    
    app.get('/statics/getstaticcontent', function (req, res) {
        res.set('Content-Type', 'application/json');
        var what = req.query.contentkey;
        res.send(common.staticdata[what]);
    });

    // ========== Transactional emails tester ===========
    app.get('/te/:cat?/:title?', function (req, res, next) {
        
        res.set('Content-Type', 'text/html');
        var content = {
            brand:          BRAND,
        };
        
        var myswig = new swig.Swig({varControls: ['{{', '}}']});
        
        var title = req.param("title")?req.param("title"):'_index';
        var cat = req.param("cat")?req.param("cat")+'/':'';
        
        var tmpl = myswig.compileFile('../public/partials/transactional-emails/'+ cat +  title + ".html.swig");

        // ACCOUNT EMAILs
        // content = utils.readJsonFileSync("../public/datadummy/affiliate.json");

        // BOOKING & TAILORMADE EMAILs
        content = utils.readJsonFileSync("../public/datadummy/mail.json");
       
        var contentmail = content;
        renderedHtml = tmpl(contentmail);

        res.end(renderedHtml);

    });


    // ========== GENERIC PAGE for traveler/ pages/page-slug ===========
    app.get('/pagina/:title?', function (req, res, next) {

        res.set('Content-Type', 'text/html');
        var pagecontent = {
            brand:          BRAND,
            canonicalurl:   filepaths.rooturl+"/pagina/"+req.param("title"),
            url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            session:        req.omtsession,
            navigationType: 'affiliate',
            login: false,
            page: {},
            getUrlCloudinary: utils._cloudinary_url,
            cloudinary : cloudinary
        };
        
        // comentar la siguiente linea a la api de produción
        var api = utils.api;

        var tmpl = swig.compileFile(filepaths.static.statictext);
        var url = api + '/cms/getPages';
        var data = { "page": req.param("title")};

        utils.http.Get(url, data, utils.headers, function (results) {
            if (results) {
                if (results.responseBody) {
                    pagecontent.page = JSON.parse(results.responseBody);

                    if (pagecontent.page.profile != null && pagecontent.page.profile != undefined){
                        for (var i = 0; i < pagecontent.page.profile.length; i++) {
                            pagecontent.page.profile[i]
                            if(pagecontent.page.profile[i].name == "Affiliate"){
                                pagecontent.navigationType = 'affiliate';
                                //console.log("Match");
                            } else {
                                //console.log("No match profile");
                                next();
                            }
                        }
                    }
                    if (pagecontent.page.specificTemplate){
                        if(pagecontent.page.templateUrl != null && pagecontent.page.templateUrl != undefined){
                            tmpl = swig.compileFile(filepaths.publicdirectory+'/'+pagecontent.page.templateUrl);
                        }
                    }
                    if (pagecontent.page._id) {
                        pagecontent.title = pagecontent.page.title;
                        var renderedHtml = tmpl(pagecontent);
                        res.end(renderedHtml);
                    }

                    else {
                        console.log("An unknown error has ocurred in responseBody");
                        next();
                    }
                }
                else {
                    console.log("An unknown error has ocurred in responseBody");
                    next();
                }
            }
            else {
                console.log("An unknown error has ocurred in results");
                next();
            }
        });

    });


    // ==========  CATEGORY / cat ===========
    app.get('/paginas/:cat?', function (req, res, next) {
        res.set('Content-Type', 'text/html');
        var pagcatcontent = {
            brand:          BRAND,
            // canonicalurl:   filepaths.rooturl + utils._getUrlBrand(req.route.path),
            canonicalurl:   filepaths.rooturl+"/paginas/"+req.param("cat"),
            // url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            session:        req.omtsession,
            navigationType: 'affiliate',
            title:          'Categoria de Páginas - ' + BRAND.domain,
            description :   'Categoria de Páginas - ' + BRAND.domain,
            substring: utils._substring,
            login: false,
            pagcat: {}
        };

        // TODO CAMBIO DE URL PARA PRODUCCION 
        // comentar la siguiente linea a la api de produción  
        var api = utils.api;

        var tmpl = swig.compileFile(filepaths.static.pagcat);
        var url = api + '/cms/getPages';
        var data = { "category": req.param("cat")};

        utils.http.Get(url, data, utils.headers, function (results) {
            if (results) {
                if (results.responseBody) {
                    pagcatcontent.pagcat = JSON.parse(results.responseBody);                
                    pagcatcontent.pagcat = pagcatcontent.pagcat[0];
                    renderedHtml = tmpl(pagcatcontent);
                    res.end(renderedHtml);
                }
                else {
                    console.log("An unknown error has ocurred in responseBody");
                    next();
                }
            }
            else {
                console.log("An unknown error has ocurred in results");
                next();
            }
        });

    });


        // ============== Private Zone ===============
    app.get('/private', function (req, res, next) {
        res.set('Content-Type', 'text/html');
        //console.log ('es private ');
        
        var content = {
            brand: BRAND,
            session: req.omtsession,    
            navigationType: '',
            redirectURL : req.param('url')
        };

        var tmpl = swig.compileFile(filepaths.static.private);
        var renderedHtml = tmpl(content);
        res.end(renderedHtml);
        
    });

};
