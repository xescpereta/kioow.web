﻿module.exports = function (app) {
    'use strict';
    //dependencies
    var utils = require('../utils');
    var filepaths = require('./diskfilepaths').filepaths;
    var _ = require('underscore');

    var settings = require('nconf');
    settings.env().file({ file: filepaths.configurationfile });

    var BRAND = settings.get('brand');

    app.get('/admin', function (req, res, next) {
        res.set('Content-Type', 'text/html');

        var content = {
            brand:          BRAND,
            language:       "es",
            title:          "Incio Administrador",
            description :   'Página pricipal de administración de' + BRAND.domain,
            url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            session:        req.omtsession,
            navigationType: 'admin'
        };

        console.log('content.session', content.session);

        if(content.session != null && content.session.user.isAdmin){
            var tmpl = swig.compileFile(filepaths.admin.home);
            var html = tmpl(content);
            res.send(html);
        } else {
            var tmpl = swig.compileFile(filepaths.static.private);
            var html = tmpl(content);
            res.send(html);
        }
    });

    app.get('/admin/affiliate-list', function (req, res, next) {
        var content = {
            brand:          BRAND,
            language:       "es",
            title:          "Listado de afiliados",
            description :   'Página de administración de afiliados de' + BRAND.domain,
            url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            session:        req.omtsession,
            navigationType: 'admin'
        };
        if(content.session != null && content.session.user.isAdmin){
            var tmpl = swig.compileFile(filepaths.admin.affiliatelist);
            var html = tmpl(content);
            res.send(html);
         } else {
            var tmpl = swig.compileFile(filepaths.static.private);
            var html = tmpl(content);
            res.send(html);
        }
    });
};