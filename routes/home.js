﻿module.exports = function (app) {

    //dependencies
    var utils = require('../utils');
    var filepaths = require('./diskfilepaths').filepaths;
    var _ = require('underscore');

    var settings = require('nconf');
    settings.env().file({ file: filepaths.configurationfile });
    var BRAND = settings.get('brand');

    var common_content = {
        brand:      settings.get('brand'),
        phone:      settings.get('phone'),
        logo:       settings.get('logo'),
        logox2:     settings.get('logox2'),
        lang:       'es',
        profileURL: '/profile'
    };

    //full index control
    app.all('*', function (req, res, next) {
        
        var nconf = require('nconf');
        nconf.env().file({ file: filepaths.configurationfile });
        
        var maintain = nconf.get('maintenance');
        if (maintain == 'true') {
            res.status(503);
            var tmpl = swig.compileFile(filepaths.static.maintenance);
            var html = tmpl();
            res.send(html);
        }
        else {
            if (req.method == 'OPTIONS') {
                console.log('options method... lets pass...');
                res.end(200);
            }
            else {
                next();
            }
        }

    });
        
     
    app.all('/apicall/*', function (req, res) {
        var call = req.path;
        
        var apiurl = call.replace('/apicall', utils.api);
        var method = req.method;
        
        if (method == 'GET') {
            var args = req.query;
            utils.http.Get(apiurl, args, utils.headers, function (results) { 
                res.send(results.responseBody);
            });
        }
        if (method == 'POST') {
            var args = req.body;
            utils.http.Post(apiurl, args, utils.headers, function (results) {
                res.send(results.responseBody);
            });
        }

        console.log(apiurl);
        console.log(req.method);
        
    });
    
    app.get('/cache', function (req, res) {
        var key = req.query.key;
        req.omtcache.methods.recover(key, function (results) { 
            //console.log(results);
            res.header("Content-Type", "application/json; charset=utf-8");
            res.header("Access-Control-Allow-Origin", "*");
            if (results != null) {
                res.send(results.Item);
            }
            else { 
                res.send(null);
            }
        });

    });
    
    app.post('/cache', function (req, res) {
        var rq = req.body;
        //console.log(results);
        req.omtcache.methods.store(rq.key, rq.item, function (results) {
            res.header("Content-Type", "application/json; charset=utf-8");
            res.header("Access-Control-Allow-Origin", "*");
            
            res.send(results);
        });

    });


    
    // ========== HOME / client-home ===========
    app.get('/', function (req, res, next) {
        res.set('Content-Type', 'text/html');

        var homecontent = {
            brand:          BRAND,
            // canonicalurl:   filepaths.rooturl + utils._getUrlBrand(req.route.path),
            // url:            filepaths.rooturl + utils._getUrlBrand(req.route.path),
            url:            filepaths.rooturl+'/',
            session:        req.omtsession,
            navigationType: 'kioow',
            title:          'Kioow pets - ' + BRAND.domain,
            description:    'Kioow pets - ' + BRAND.domain,
            googlePlusApi:  filepaths.googlePlusApi,
            facebookAppId:  filepaths.facebookAppId
        };

        var tmpl = swig.compileFile(filepaths.home);
        //var tmpl = swig.compileFile(filepaths.static.maintenance);
        renderedHtml = tmpl(homecontent);
        res.end(renderedHtml);

    });


    app.get('/toursbytag/:tag?/:page?', function (req, res, next) {
        
        var tag = req.param("tag");
        var page = req.param("page");
        if (page === undefined) {
            page = 0;
        }
        var inspirateresults = {};

        var api = utils.api;
        var url = api + '/api/inspirationalResults';
        var data = { "tag": req.param("tag"), "b2cchannel": true, "currency" : "EUR"};

        utils.http.Get(url, data, utils.headers, function (results) {
            if (results) {
                if (results.responseBody) {
                    inspirateresults = results.responseBody;
                    //console.log(inspirateresults)
                    // resultgroups = inspirateresults.Item.Pages[page].items;
                    res.json(inspirateresults);
                }
                else {
                    console.log("An unknown error has ocurred in responseBody");
                    next();
                }
            }
            else {
                console.log("An unknown error has ocurred in results");
                next();
            }
        });
        // var inspirateresults = utils.getJson(filepaths.data.dummy.inspirate);
        //http://openmarketdev.cloudapp.net:3000/api/inspirationalResults?tag=art&b2bchannel=false&currency=%E2%82%AC%20euro
        // var resultgroups = eurodummy.Item.Pages[page].items;
        // res.json(resultgroups);   
    }); 
    
    app.get('/data/slider', function (req, res, next) {
        getPromotionData(req, res, next, function(data){
            res.json(data);
        });
    }); 

    var getPromotionData = function (req, res, next, callback) {
        var productCodes = [];
        var productsHome = [];
        var api = utils.api;
        
        
        req.omtcache.methods.recover('productshowhome', function (results) {
            
            function isEmpty(obj) {
                if (obj != null) {
                    return Object.keys(obj).length === 0;
                }
                else { return true; }    
            }
            //
            console.log('isEmpty(results)',isEmpty(results));
            //
            if(results == null || isEmpty(results)){
                console.log("regenerar...");
                var urlkeystone = api + '/cms/getPromotions';
                //var urlkeystone = 'http://localhost:3000/cms/getPromotions';
                utils.http.Get(urlkeystone, "", utils.headers, function (results) {
                    if (results) {
                        if (results.responseBody) {
                            var rawproductCodes = JSON.parse(results.responseBody);
                            var dateActual = new Date();
                            for (var i = rawproductCodes.length - 1; i >= 0; i--) {

                                if (rawproductCodes[i].state=='published'){
                                   // console.log("fechas : act",dateActual," db:",new Date(rawproductCodes[i].finishAt)," res : ",rawproductCodes[i].finishAt >= dateActual)
                                    if ( new Date(rawproductCodes[i].finishAt) >= dateActual){
                                        //console.log("hago push");
                                        productCodes.push(rawproductCodes[i])
                                    }
                                }
                            };
                            function compare(a, b) {
                                if (a.sortOrder < b.sortOrder)
                                    return -1;
                                if (a.sortOrder > b.sortOrder)
                                    return 1;
                                return 0;
                            }
                            productCodes.sort(compare);
                            _searchProduct();
                        } else {
                            //res.send(null);
                            if(callback){
                                callback(null)
                            }
                        };
                    } else { 
                        //res.send(null);
                        if(callback){
                                callback(null)
                            }
                    };
                });

                function _searchProduct() {
                    console.log('start searching codes...');
                    // console.log(productCodes);
                    var url = api + '/api/getproduct';
                    var iterator = 0;
                    var looper = 0;
                    if (productCodes != null && productCodes.length > 0) {
                        for (var i = 0; i < productCodes.length; i++) {
                            iterator++
                            var data = { "code": productCodes[i].code, "currency" : "EUR", "zoom" : productCodes[i].zoom };
                            utils.http.Get(url, data, utils.headers, function (results) {
                                if (results) {
                                    if (results.responseBody) {
                                        var resp = JSON.parse(results.responseBody);
                                        if (resp.publishState == 'published') {
                                            var dummy = {};
                                            dummy.markers = utils._getMarkers(resp.itinerary);
                                            if (dummy.markers.length > 0) {
                                                dummy.code = resp.code;
                                                dummy.slug = resp.slug_es;
                                                for (var j = productCodes.length - 1; j >= 0; j--) {
                                                    if (resp.code == productCodes[j].code) {
                                                        dummy.zoom = productCodes[j].zoom;
                                                    }
                                                }                                                ;
                                                
                                                dummy.title_es = resp.title_es;
                                                dummy.itinerary = [];
                                                //
                                                var tempcities = utils._showCities(resp.itinerary);
                                                dummy.cities = utils._cityReducerHome(tempcities, 2);
                                                //console.log(dummy.cities);
                                                dummy.days = resp.itinerary.length;
                                                if (resp.tags.length > 3) {
                                                    dummy.tags = resp.tags.slice(0, 3);
                                                    dummy.tags.push({ label : '...' });
                                                } else {
                                                    dummy.tags = resp.tags
                                                }
                                                
                                                //var from = new Date();
                                                //var to = new Date(from.getFullYear() + 1, from.getMonth(), from.getDate());
                                                //console.log("form :"+from+ " to: "+to );
                                                //dummy.price = utils._calculatePriceMinimumDates(from, to, resp.availability);
                                                if (resp.minprice.currency.value == 'EUR'){
                                                    dummy.price = resp.minprice;
                                                } else {
                                                    dummy.price = resp.minprice.exchange;
                                                }
                                                //dummy.price = resp.minprice;
                                                dummy.markers = utils._getMarkers(resp.itinerary);
                                                //
                                                var position = 0;
                                                for (var k = 0; k < productCodes.length; k++) {
                                                    if (productCodes[k].code == dummy.code) {
                                                        position = k;
                                                    }
                                                };
                                                productsHome[position] = dummy;
                                            //

                                            } else {
                                                console.log("the product : " + resp.code + " has no markers")
                                            }
                                        } else {
                                            console.log("the product : " + resp.code + " is unpublished")
                                        }
                                   
                                    } else {
                                        console.log("An unknown error has ocurred in responseBody productshowhome");
                                    //next();
                                    }
                                } else {
                                    console.log("An unknown error has ocurred in results productshowhome");
                                //next();
                                }
                                _getCacheComplete();
                            }); // end requests
                        
                        } // end for
                        
                        var _getCacheComplete = function () {
                            looper++
                            if (looper == iterator) {
                                // remove null objects
                                var finalProducts = [];
                                for (var l = 0; l < productsHome.length; l++) {
                                    if (productsHome[l] != null) {
                                        finalProducts.push(productsHome[l]);
                                    }
                                }
                                req.omtcache.methods.store('productshowhome', finalProducts, function (results) {
                                    //cache regenerada
                                    console.log("set cache home products");
                                    //res.json(finalProducts);
                                    if(callback){
                                        callback(finalProducts);
                                    }
                                });
                            }
                        };
                    } else { 
                        //res.send(null);
                        if(callback){
                            callback(null);
                        }
                    }
                    
                } //end function _searchProduct

            }else{
               // console.log("·por aqui·")
               // console.log(results.Item);
               //res.send(results.Item);
               if(callback){
                    callback(results.Item);
                }
            }
        });
    };

    console.log('loading home routes.. [OK]');

};




