﻿var utils = require('../utils');
var generator = function () {
    

    function _recover(key, callback) {
        //dependencies
        
        //recover token from request
        //console.log(request.cookies);
        var token = key;
        
        var nosession = {
            ResultOK: false,
            Message: 'No cache found.'
        };
        var StorageRequest = {
            key : key,
            Item : null
        };
        if (token != null && token != '') {
            var url = 'http://test.openmarket.travel:3003/api/recoverstore';
            //var url = 'http://localhost:3003/api/recoverstore';
            utils.http.Get(url, StorageRequest, utils.headers, function (results) {
                
                if (results) {
                    var item = results.responseBody;
                    //console.log(item);
                    if (item != null && item != '') {
                        callback(JSON.parse(item));
                    }
                    else {
                        callback(null);
                    }
                    //callback(JSON.parse(item));
                }
                else {
                    callback(nosession);
                }
            });
        } else {
            callback(null);
        }
    }
    


    function generate_json_urls(callback) {
        

        var load = {
            products : false,
            triptags : false,
            dmc : false,
            country : false,
            base : false
        }
        var root_path = 'http://www.openmarket.travel';
        var urls = [];
        // model obj to site map : 
        var dummyUrl = {
            loc : '',
            priority : 0.5,
            changefreq : 'monthly',
            lastmod : new Date()
        }
        
        
        function loadBaseUrl() {
            var homesURLs = [
                '/',
                '/viajes',
                '/viaje-a-medida',
                '/pagina/openmarkettravel'
            ];
            var contentURLs = [
                '/preguntas-frecuentes'
            ]
            for (var i = 0; i < homesURLs.length; i++) {
                var dummyUrl = homesURLs[i];
                urls.push(dummyUrl);
            };
            for (var i = 0; i < contentURLs.length; i++) {
                var dummyUrl = contentURLs[i];
                urls.push(dummyUrl);
            }            ;
            console.log("ok base ..");
            load.base = true;
            // call to check complete
            checkCompleteLoad();
        }
        // lunch base 
        loadBaseUrl();
        

        //get countrys 
        _recover('CMSCountriesCACHE', function (results) {
            
            if (results == null) {
                
                console.log("ERROR we must to regenerate CMSCountriesCACHE");

            } else {
                
                if (results != null && results.Item != null && results.Item.items != null) {
                    var obj = results.Item.items;
                    for (var key in obj) {
                        var attrName = key;
                        var attrValue = obj[key];
                        var lbl = obj[key].label_es;
                        var dummyUrl = "/viajes/" + lbl + "/?country=" + attrName;
                        console.log(dummyUrl);
                        urls.push(dummyUrl);
                    }
                }
                load.country = true;
                console.log("ok countries ..");
                // call to check complete
                checkCompleteLoad();
            }
        });
        
        //get triptags 
        _recover('CMSTripTagsCACHE', function (results) {
            
            if (results == null) {
                
                console.log("ERROR we must to regenerate CMSTripTagsCACHE");

            } else {
                
                
                if (results != null && results.Item != null && results.Item.items != null) {
                    var obj = results.Item.items;
                    for (var key in obj) {
                        var attrName = key;
                        var attrValue = obj[key];
                        //var lbl = obj[key].label_es;
                        var dummyUrl = "/viajes/?tags=" + attrName;
                        console.log(dummyUrl);
                        urls.push(dummyUrl);
                    }
                }
                load.triptags = true;
                console.log("ok triptags ..");
                // call to check complete
                checkCompleteLoad();
            }
        });
        
        _recover('ProductsCACHE', function (results) {
            
            if (results == null) {
                
                console.log("ERROR we must to regenerate PublishedProductsCACHE");

            } else {
                
                if (results != null && results.Item != null && results.Item.length > 0) {
                    var pages = results.Item.length;
                    // pages for
                    for (var i = 0; i < pages; i++) {
                        // items for
                        var item = results.Item[i];
                        var slug = item.slug_es
                        var dummyUrl = "/viaje/" + slug;
                        console.log(dummyUrl);
                        urls.push(dummyUrl);
                    } // end for pages
                }
                load.products = true;
                console.log("ok product ..");
                // call to check complete
                checkCompleteLoad();
            }
        });
        
        _recover('DMCsCACHE', function (results) {
            
            if (results == null) {
                
                console.log("ERROR we must to regenerate DMCsCACHE");

            } else {
                
                if (results != null && results.Item != null) {
                    // items for
                    for (var i = 0; i < results.Item.length; i++) {
                        var item = results.Item[i]
                        var slug = item.code;
                        var name = encodeURIComponent(item.name);
                        var dummyUrl =  "/receptivo/" + slug + "/"+ name;
                        console.log(dummyUrl);
                        urls.push(dummyUrl);
                    } // end for item
                }
                load.dmc = true;
                console.log("..... ok dmc .....");
                // call to check complete
                checkCompleteLoad();
            }
        });
        
        function checkCompleteLoad() {
            if (load.base && load.country && load.triptags && load.products && load.dmc) {
                callback(urls);
            }            
        }        
        

    }


    this.generate = function () {
        generate_json_urls(function (json) {
            

            function writeFile(filepath, data, onwritefinishhandler) {
                var fs = require('fs');
                fs.writeFile(filepath, data, function (err) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    
                    if (onwritefinishhandler) {
                        onwritefinishhandler();
                    }
                });
            }
            var cant = json.length;
            var json = JSON.stringify(json);
            //REMEMBER CHANGE THE SAVING ROUTE FOR THE SITEMAP.XML FILE!!! ***** IMPORTANT!!!
            //writeFile('C:/development/node/openmarket/public/omturl.json', json, 
            writeFile('../omturl.json', json,   
                function () {
                console.log(cant+' urls in omt list generated, - [OK] at ' + new Date());
            });
        });
    }

}

var gn = new generator();
gn.generate();