﻿var utils = require('../utils');
var generator = function () {
    
    var google = 'http://www.google.com/webmasters/tools/ping?sitemap=' + escape('http://www.openmarket.travel/sitemap.xml');
    var bing = 'http://www.bing.com/webmaster/ping.aspx?siteMap=' + escape('http://www.openmarket.travel/sitemap.xml');
    
    function sendtosearchengines(tools) {
        tools.http.Get(google, null, null, function (results) { 
            console.log(results.responseBody);
        });
        tools.http.Get(bing, null, null, function (results) { 
            console.log(results.responseBody);
        });
    }

    function _recover(key, callback) {
        //dependencies
        
        //recover token from request
        //console.log(request.cookies);
        var token = key;
        
        var nosession = {
            ResultOK: false,
            Message: 'No cache found.'
        };
        var StorageRequest = {
            key : key,
            Item : null
        };
        if (token != null && token != '') {
            var url = 'http://localhost:3003/api/recoverstore';

            utils.http.Get(url, StorageRequest, utils.headers, function (results) {
                
                if (results) {
                    var item = results.responseBody;
                    //console.log(item);
                    if (item != null && item != '') {
                        callback(JSON.parse(item));
                    }
                    else {
                        callback(null);
                    }
                    //callback(JSON.parse(item));
                }
                else {
                    callback(nosession);
                }
            });
        } else {
            callback(null);
        }
    }
    


    function generate_xml_sitemap(callback) {
        

        var load = {
            products : false,
            triptags : false,
            dmc : false,
            country : false,
            base : false
        }
        var root_path = 'http://www.openmarket.travel';
        var urls = [];
        // model obj to site map : 
        var dummyUrl = {
            loc : '',
            priority : 0.5,
            changefreq : 'monthly',
            lastmod : new Date()
        }
        
        
        function loadBaseUrl() {
            var homesURLs = [
                '/',
                '/viajes',
                '/viaje-a-medida',
                '/pagina/openmarkettravel'
            ];
            var contentURLs = [
                '/preguntas-frecuentes'
            ]
            for (var i = 0; i < homesURLs.length; i++) {
                var dummyUrl = {
                    loc : homesURLs[i],
                    priority : 1,
                    changefreq : 'daily'
                }
                urls.push(dummyUrl);
            }            ;
            for (var i = 0; i < contentURLs.length; i++) {
                var dummyUrl = {
                    loc : contentURLs[i],
                    priority : 0.5,
                    changefreq : 'monthly'
                }
                urls.push(dummyUrl);
            }            ;
            console.log("ok base ..");
            load.base = true;
            // call to check complete
            checkCompleteLoad();
        }
        // lunch base 
        loadBaseUrl();
        

        //get countrys 
        _recover('CMSCountriesCACHE', function (results) {
            
            if (results == null) {
                
                console.log("ERROR we must to regenerate CMSCountriesCACHE");

            } else {
                
                
                if (results != null && results.Item != null && results.Item.items != null) {
                    var obj = results.Item.items;
                    for (var key in obj) {
                        var attrName = key;
                        var attrValue = obj[key];
                        var lbl = obj[key].label_es;
                        var dummyUrl = {
                            loc : "/viajes/" + lbl + "/?country=" + attrName,
                            priority : 0.7,
                            changefreq : 'daily'
                        }
                        if (obj[key].publishedDate != null) {
                            //dummyUrl.lastmod = obj[key].publishedDate;
                            var dt = new Date(obj[key].publishedDate);
                            dummyUrl.lastmod = dt.toISOString();
                        } else {
                            dummyUrl.lastmod = new Date();
                            var dt = new Date();
                            dummyUrl.lastmod = dt.toISOString();
                        }
                        //console.log(dummyUrl);
                        urls.push(dummyUrl);
                    }
                }
                load.country = true;
                console.log("ok countries ..");
                // call to check complete
                checkCompleteLoad();
            }
        });
        
        //get triptags 
        _recover('CMSTripTagsCACHE', function (results) {
            
            if (results == null) {
                
                console.log("ERROR we must to regenerate CMSTripTagsCACHE");

            } else {
                
                
                if (results != null && results.Item != null && results.Item.items != null) {
                    var obj = results.Item.items;
                    for (var key in obj) {
                        var attrName = key;
                        var attrValue = obj[key];
                        //var lbl = obj[key].label_es;
                        var dummyUrl = {
                            loc : "/viajes/?tags=" + attrName,
                            priority : 0.7,
                            changefreq : 'daily'
                        }
                        if (obj[key].publishedDate != null) {
                            var dt = new Date(obj[key].publishedDate);
                            dummyUrl.lastmod = dt.toISOString();
                        } else {
                            dummyUrl.lastmod = new Date();
                            var dt = new Date();
                            dummyUrl.lastmod = dt.toISOString();
                        }
                        //console.log(dummyUrl);
                        urls.push(dummyUrl);
                    }
                }
                load.triptags = true;
                console.log("ok triptags ..");
                // call to check complete
                checkCompleteLoad();
            }
        });
        
        _recover('ProductsCACHE', function (results) {
            
            if (results == null) {
                
                console.log("ERROR we must to regenerate PublishedProductsCACHE");

            } else {
                
                if (results != null && results.Item != null && results.Item.length > 0) {
                    var pages = results.Item.length;
                    // pages for
                    for (var i = 0; i < pages; i++) {
                        // items for
                        var item = results.Item[i];
                        var slug = item.slug_es
                        var dummyUrl = {
                            loc : "/viaje/" + slug,
                            priority : 0.9,
                            changefreq : 'daily'
                        }
                        if (item.updatedOn != null) {
                            dummyUrl.lastmod = item.updatedOn;
                        } else if (item.createdOn != null) {
                            dummyUrl.lastmod = item.createdOn;
                        } else {
                            var dt = new Date();
                            dummyUrl.lastmod = dt.toISOString();
                        }
                        //console.log(dummyUrl);
                        urls.push(dummyUrl);
                    } // end for pages
                }
                load.products = true;
                console.log("ok product ..");
                // call to check complete
                checkCompleteLoad();
            }
        });
        
        _recover('DMCsCACHE', function (results) {
            
            if (results == null) {
                
                console.log("ERROR we must to regenerate DMCsCACHE");

            } else {
                
                if (results != null && results.Item != null) {
                    // items for
                    for (var i = 0; i < results.Item.length; i++) {
                        var item = results.Item[i]
                        var slug = item.code;
                        var name = encodeURIComponent(item.name);
                        var dummyUrl = {
                            loc : "/receptivo/" + slug + "/"+ name,
                            priority : 0.6,
                            changefreq : 'monthly'
                        }
                        if (item.updatedOn != null) {
                            dummyUrl.lastmod = item.updatedOn;
                        } else if (item.createdOn != null) {
                            dummyUrl.lastmod = item.createdOn;
                        } else {
                            dummyUrl.lastmod = new Date();
                        }
                        //console.log(dummyUrl);
                        urls.push(dummyUrl);
                    } // end for item
                }
                load.dmc = true;
                console.log("ok dmc ..");
                // call to check complete
                checkCompleteLoad();
            }
        });
        
        function checkCompleteLoad() {
            if (load.base && load.country && load.triptags && load.products && load.dmc) {
                var xml = writeXML();
                callback(xml);
            }            
        }        
        
        

        function writeXML() {
            // XML sitemap generation starts here
            var xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            for (var i in urls) {
                xml += '<url>';
                xml += '<loc>' + root_path + urls[i].loc + '</loc>';
                xml += '<changefreq>' + urls[i].changefreq + '</changefreq>';
                if (urls[i].lastmod != null) {
                    //YYYY-MM-DDThh:mmTZD 
                    xml += '<lastmod>' + urls[i].lastmod + '</lastmod>';
                }
                xml += '<priority>' + urls[i].priority + '</priority>';
                xml += '</url>';
                i++;
            }
            xml += '</urlset>';
            return xml;
        }

    }


    this.generate = function () {
        generate_xml_sitemap(function (xml) {
            

            function writeFile(filepath, data, onwritefinishhandler) {
                var fs = require('fs');
                fs.writeFile(filepath, data, function (err) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    
                    if (onwritefinishhandler) {
                        onwritefinishhandler();
                    }
                });
            }
            //REMEMBER CHANGE THE SAVING ROUTE FOR THE SITEMAP.XML FILE!!! ***** IMPORTANT!!!
            writeFile('C:/development/node/openmarket/public/sitemap.xml', xml, 
                function () {
                console.log('SiteMAP Generated - [OK] at ' + new Date());
                sendtosearchengines(utils);
            });
        });
    }

}

var gn = new generator();
gn.generate();